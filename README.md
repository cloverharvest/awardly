# README

Remember:  
1. Model testing - checks on validations and simple connections  
2. Controller testing - controllers are the key piece connecting data with its users  
- controllers support a number of complex interactions
- controller need tests that can examine the actions they were supposed to perform  
3. If you get this warning when testing controllers:

`DEPRECATION WARNING: ActionController::TestCase HTTP request methods will accept only
keyword arguments in future Rails versions.`

Know that there is no solution to this yet by the Rails Team, to get rid of that warning for Rails 5:

https://github.com/thoughtbot/shoulda-matchers/issues/867#issuecomment-234880653

While it may be an "annoyance" it is not an error.

As a workaround, wrap all id properties in `params: {}`

You will now show the passing clean test.

` 2016-11-02 11:03:19 ☆  AnnaSchwabs-MacBook-Pro in ~/wdi/learn_rails_5/awardly
± |test_for_awards_controller U:2 ✗| → rake test TEST=test/controllers/courses_controller_test.rb
Run options: --seed 61453

# Running:

.......

Finished in 0.386008s, 18.1343 runs/s, 33.6780 assertions/s.

7 runs, 13 assertions, 0 failures, 0 errors, 0 skips`  

4. Go back to Chapter 14 (More Options Section) to finish authentication extensions as added features and further study.  
