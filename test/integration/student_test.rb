require 'test_helper'

class StudentTest < ActionDispatch::IntegrationTest
  def test_adding_a_student
    get new_student_path #'/students/new'

    assert_select "input[type=text][name='student[given_name]']"
    assert_select "input[type=text][name='student[family_name]']"

    assert_difference('Student.count') do
      post '/students', params: { student: {
        given_name: "Fred",
        family_name: "Smith",
        date_of_birth: "1999-09-01",
        grade_point_average: 2.0,
        start_date: "2008-09-01"
      } }
    end

    assert_redirected_to "/students/#{assigns(:student).id}"
    follow_redirect!

    assert_select "p", /Fred/
    assert_select "p", /2008\-09\-01/
  end
end
