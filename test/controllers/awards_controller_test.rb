require 'test_helper'

class AwardsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index, params: { student_id: students(:magnum).id }
    assert_response :success
    assert_not_nil assigns(:awards)
  end

  def test_should_get_new
    get :new, params: { student_id: students(:magnum).id }
    assert_response :success
  end

  def test_should_create_award
    assert_difference('Award.count') do
      post :create, params: { award: { year: 2008, name: 'Test award' },
                          student_id: students(:magnum).id }
    end

  end

  def test_should_show_award
    get :show, params: { id: awards(:pi).id, student_id: students(:magnum).id }
    assert_response :success
  end

  def test_should_get_edit
    get :edit, params: { id: awards(:pi).id, student_id: students(:magnum).id }
    assert_response :success
  end

  def test_should_update_award
    put :update, params: { id: awards(:pi).id, award: { year: 2008, name: 'Test award' },
    student_id: students(:magnum).id }
    assert_redirected_to student_awards_url(students(:magnum))
  end

  def test_should_destroy_award
    assert_difference('Award.count', -1) do
      delete :destroy,  params: { id: awards(:pi).id,student_id:
                          students(:magnum).id }
    end

    assert_redirected_to student_awards_path(students(:magnum))
  end
end
