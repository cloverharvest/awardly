Rails.application.routes.draw do
  # root :to => "students#index"

  get 'signup', to: 'users#new', as: 'signup'

  get 'login', to: 'sessions#new', as: 'login'

  get 'logout', to: 'sessions#destroy', as: 'logout'

  resources :sessions

  resources :users

  resources :courses do
    member do
      get :roll
    end
  end

  resources :students do
    resources :awards

    member do
      get :courses
      post :course_add
      post :course_remove
    end
  end

  root :to => "students#index"
#
#   Prefix Verb   URI Pattern                                     Controller#Action
#   signup          GET    /signup(.:format)                               users#new
#    login          GET    /login(.:format)                                sessions#new
#   logout          GET    /logout(.:format)                               sessions#destroy
# sessions          GET    /sessions(.:format)                             sessions#index
#                   POST   /sessions(.:format)                             sessions#create
# new_session       GET    /sessions/new(.:format)                         sessions#new
# edit_session  GET    /sessions/:id/edit(.:format)                    sessions#edit
#  session      GET    /sessions/:id(.:format)                         sessions#show
#               PATCH  /sessions/:id(.:format)                         sessions#update
#               PUT    /sessions/:id(.:format)                         sessions#update
#               DELETE /sessions/:id(.:format)                         sessions#destroy
#    users      GET    /users(.:format)                                users#index
#               POST   /users(.:format)                                users#create
# new_user      GET    /users/new(.:format)                            users#new
# edit_user     GET    /users/:id/edit(.:format)                       users#edit
#     user      GET    /users/:id(.:format)                            users#show
#               PATCH  /users/:id(.:format)                            users#update
#               PUT    /users/:id(.:format)                            users#update
#               DELETE /users/:id(.:format)                            users#destroy
# roll_course   GET    /courses/:id/roll(.:format)                     courses#roll
#  courses      GET    /courses(.:format)                              courses#index
#               POST   /courses(.:format)                              courses#create
# new_course    GET    /courses/new(.:format)                          courses#new
# edit_course   GET    /courses/:id/edit(.:format)                     courses#edit
#   course      GET    /courses/:id(.:format)                          courses#show
#               PATCH  /courses/:id(.:format)                          courses#update
#               PUT    /courses/:id(.:format)                          courses#update
#               DELETE /courses/:id(.:format)                          courses#destroy
# student_awards GET    /students/:student_id/awards(.:format)          awards#index
#               POST   /students/:student_id/awards(.:format)          awards#create
# new_student_award GET    /students/:student_id/awards/new(.:format)      awards#new
# edit_student_award GET    /students/:student_id/awards/:id/edit(.:format) awards#edit
# student_award GET    /students/:student_id/awards/:id(.:format)      awards#show
#          PATCH  /students/:student_id/awards/:id(.:format)      awards#update
#          PUT    /students/:student_id/awards/:id(.:format)      awards#update
#          DELETE /students/:student_id/awards/:id(.:format)      awards#destroy
# courses_student GET    /students/:id/courses(.:format)                 students#courses
# course_add_student POST   /students/:id/course_add(.:format)              students#course_add
# course_remove_student POST   /students/:id/course_remove(.:format)           students#course_remove
# students GET    /students(.:format)                             students#index
#          POST   /students(.:format)                             students#create
# new_student GET    /students/new(.:format)                         students#new
# edit_student GET    /students/:id/edit(.:format)                    students#edit
#  student GET    /students/:id(.:format)                         students#show
#          PATCH  /students/:id(.:format)                         students#update
#          PUT    /students/:id(.:format)                         students#update
#          DELETE /students/:id(.:format)                         students#destroy
#     root GET    /
end
