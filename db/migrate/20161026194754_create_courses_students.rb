class CreateCoursesStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :courses_students, id: false do |t|
      t.integer :course_id, null: false
      t.integer :student_id, null: false
    end

    # add index to speed lookup through course id and student id,
    # unique: true => makes sure that student gets enrolled only once in each course

    add_index :courses_students, [:course_id, :student_id], unique: true
  end
end
