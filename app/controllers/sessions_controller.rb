class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "Logged in!"
    else
      flash.now.notice = "Email or password is invalid"
      render "new"
    end
  end

  def destroy
    # set the user_id to nil and then
    session[:user_id] = nil
    # redirect user to root and display Logged out!
    redirect_to root_url, notice: "Logged out!"
  end
end
