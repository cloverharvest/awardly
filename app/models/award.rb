class Award < ApplicationRecord
  # an award can be linked to a student, via a student_id (foreign_key)
  belongs_to :student
  validates_associated :student

  validates_presence_of :name, :year

 # award can be given only once a year
  validates_uniqueness_of :name, scope: :year

 # award scheme was started in 1980
  validates_inclusion_of :year, in: (1980..Date.today.year)
end
